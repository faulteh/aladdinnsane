FROM tensorflow/tensorflow:2.2.0
RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install -y python3-pip python3-dev python3-venv

RUN mkdir -p /app
COPY . /app

RUN pip3 install -r /app/requirements.txt

WORKDIR /app
ENTRYPOINT ["/app/docker-entrypoint.sh"]
