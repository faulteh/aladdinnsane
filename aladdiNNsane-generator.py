#!/usr/bin/env python3
"""
This is the trainer for the NN
"""
import numpy as np
import pandas as pd
import sqlite3
import sys 
from textgenrnn import textgenrnn
import os
import argparse

parser = argparse.ArgumentParser(description='Train a textgen model')
parser.add_argument('--samples', type=int, default=100)
parser.add_argument('--model', default='training/bowie/500nds_12Lrs_100epchs_Model')

args = parser.parse_args()

weights_file = '%s_weights.hdf5' % args.model
config_file = '%s_config.json' % args.model
vocab_file = '%s_vocab.json' % args.model

textgen = textgenrnn(weights_path=weights_file, vocab_path=vocab_file, config_path=config_file)

generated = textgen.generate_samples(args.samples)

print(generated)
