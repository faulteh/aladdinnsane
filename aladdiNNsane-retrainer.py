#!/usr/bin/env python3
"""
This is the trainer for the NN - takes existing model and trains against another text
"""
import numpy as np
import pandas as pd
import sqlite3
import sys 
from textgenrnn import textgenrnn
import os
import argparse

parser = argparse.ArgumentParser(description='Train a textgen model')
parser.add_argument('--text')
parser.add_argument('--model', default='training/500nds_12Lrs_100epchs_Model')
parser.add_argument('--epochs', type=int, default=100)
parser.add_argument('--genepochs', type=int, default=25)
args = parser.parse_args()

weights_file = '%s_weights.hdf5' % args.model
config_file = '%s_config.json' % args.model
vocab_file = '%s_vocab.json' % args.model

textgen = textgenrnn(weights_path=weights_file, vocab_path=vocab_file, config_path=config_file)

with open(args.text, 'r') as f:
    lines = f.readlines()

textgen.train_on_texts(lines, num_epochs=args.epochs, gen_epochs=args.genepochs)
