#!/bin/bash
set -e
if [ "$1" = "redispub" ]; then
	cd /app
	./aladdiNNsane-redispub.py --model $2 --redis $3 --rkey $4
fi
exec "$@"
