#!/usr/bin/env python3
"""
This is the trainer for the NN
"""
from typing import List
import sys 
from textgenrnn import textgenrnn
import os
import argparse
import redis
import time

parser = argparse.ArgumentParser(description='Train a textgen model')
parser.add_argument('--samples', type=int, default=100)
parser.add_argument('--model', default='training/bowie/500nds_12Lrs_100epchs_Model')
parser.add_argument('--redis', default='redis://redis:6379/0')
parser.add_argument('--rkey', default='aladdinnsane')
parser.add_argument('--temperature', type=float, default=0.5)

args = parser.parse_args()

weights_file = '%s_weights.hdf5' % args.model
config_file = '%s_config.json' % args.model
vocab_file = '%s_vocab.json' % args.model

textgen = textgenrnn(weights_path=weights_file, vocab_path=vocab_file, config_path=config_file)

rconn = redis.from_url(args.redis)

def samples_in_redis() -> int:
    """
    Check if key exists in redis, and get the length of the queue in redis
    """
    if rconn.exists(args.rkey) == 1:
        return rconn.llen(args.rkey)
    else:
        return 0

def get_more_samples() -> List[str]:
    """
    Run the text generator and get more samples
    """
    generated: List[str] = []
    while len(generated) == 0:
        generated = textgen.generate(
            n=args.samples,
            progress=False,
            temperature=args.temperature,
            return_as_list=True)
        generated = [g for g in generated if g != '']
    return generated

while True:
    if samples_in_redis() < 20:
        samples = get_more_samples()
        for sample in samples:
            rconn.lpush(args.rkey, sample)
    time.sleep(5)
